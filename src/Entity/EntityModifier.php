<?php

namespace Drupal\remove_entity_untranslatable_field_validation\Entity;

/**
 * Alters entities.
 *
 * @package Drupal\remove_entity_untranslatable_field_validation\Entity
 */
class EntityModifier {

  /**
   * Removes entity untranslatable field validation from entities.
   *
   * @param array $entityTypes
   *   Entity types from hook_entity_type_alter().
   */
  public function removeUntranslatableEntityFieldValidation(array &$entityTypes) {
    foreach ($entityTypes as $entityType) {
      $constraints = $entityType->getConstraints();
      unset($constraints['EntityUntranslatableFields']);
      $entityType->setConstraints($constraints);
    }
  }

}
